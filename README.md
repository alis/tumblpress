# TumblPress

Imports Tumblr posts into WordPress, and crossposts WordPress posts to Tumblr. Also adds options to "clean up" Tumblr crossposts (delete older than *n* days) and likes.

**Warning:** My (paid!) Tumblr account deadass got banned and I'm 70% sure it was from using this plugin, so use at your own risk.

## Requirements

**1.** [Composer](https://github.com/composer/composer).

**2.** The [PHP OAuth class](http://php.net/manual/en/book.oauth.php).

**3.** A [Tumblr API key](https://www.tumblr.com/oauth/apps).

## Installation

**1.** Upload files to your `wp-content/plugins/tumblrpress` directory.

**2.** Copy `tumblr.class.php-example` to `tumblr.class.php`.

**3.** Edit `tumblr.class.php` and change `$conskey` and `$conssec` to the values obtained in *Requirements #2*.

**4.** Run `composer update` (or [whatever](https://getcomposer.org/doc/01-basic-usage.md#updating-dependencies-to-their-latest-versions)) in `wp-content/plugins/tumblrpress`.

**5.** Enjoy!